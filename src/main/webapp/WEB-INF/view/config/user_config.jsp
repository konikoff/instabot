<!DOCTYPE html>

<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/view/header.jsp"/>

<link rel="stylesheet" type="text/css" href="mystyle.css">

<link href="${pageContext.request.contextPath}/resources/css/config/user_config.css" rel="stylesheet"/>

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">User details</div>
    <div class="panel-body">
        <div class="col-xs-5">
            <div class="panel panel-default">
                <div class="panel-heading">User information</div>
                <div class="panel-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Name</td>
                            <td>
                                <input type="text" class="form-control" placeholder="Name" value="${user_name}">
                            </td>
                        </tr>
                        <tr>
                            <td>Surname</td>
                            <td>
                                <input type="text" class="form-control" placeholder="Surname" value="${user_surname}">
                            </td>
                        </tr>
                        <tr>
                            <td>E-mail</td>
                            <td>
                                <input type="email" class="form-control" placeholder="email" value="${user_email}">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-default">Change information</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-5">
            <div class="panel panel-default">
                <div class="panel-heading">User credentials</div>
                <div class="panel-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Login</td>
                            <td>
                                <input type="text" class="form-control" placeholder="Login" value="${user_login}" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>New Password</td>
                            <td>
                                <input type="password" class="form-control" placeholder="Type new password">
                            </td>
                        </tr>
                        <tr>
                            <td>Repeat password</td>
                            <td>
                                <input type="password" class="form-control" placeholder="Repeat new password">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-default">Change password</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
