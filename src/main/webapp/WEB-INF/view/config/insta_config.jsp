<!DOCTYPE html>

<%@ page isELIgnored="false" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/view/header.jsp"/>


<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">User details</div>
    <div class="panel-body">
        <div class="col-xs-5" id="user-details">
            <div class="panel panel-default">
                <div class="panel-heading">Instagram authorization:</div>
                <div class="panel-body">
                    <table class="table table-borderless" id="instagram-auth-form">
                        <tr>
                            <td>Login</td>
                            <td>
                                <input type="text" class="form-control" placeholder="Login" id="insta-login">
                            </td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td>
                                <input type="text" class="form-control" placeholder="Password" id="insta-password">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-default" id="btn-insta-signin">Sign in</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.serializeObject.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/stomp/sockjs-0.3.4.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/stomp/stomp.js" type="text/javascript"></script>

<script type="text/javascript">
    window.onload = connect;

    var delay = function () {
        //getAuthorizedInstagramAccounts();
        setTimeout(getAuthorizedInstagramAccounts, 1000);
    };
    delay();

    var stompClient = null;

    function connect() {
        var socket = new SockJS('/web-service');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);

            stompClient.subscribe('/topic/instagram/auth', function (response) {
                var message = JSON.parse(response.body);
                // Status code 200 - successful
                if (message.statusCode === 200) {
                    getAuthorizedInstagramAccounts();
                } else {
                    unauthorizedRequest(message);
                }
            });
            stompClient.subscribe("/user/queue/instagram/authorizedAccounts", function (response) {
                var accounts = JSON.parse(response.body);

                if (accounts instanceof Array) {
                    if (accounts.length > 0) {
                        if ($('#authorized-accounts').length === 0) {
                            addAuthorizedAccountsTable();
                        }


                        $('#accounts-table').empty();
                        accounts.forEach(function (account, i, accounts) {
                            addAthorizedRow(account);
                        });
                    }
                }
            });
        });
    }

    function getAuthorizedInstagramAccounts() {
        $.get("/instagramAuthorizedAccounts", function (response) {
            console.log(response.body);
        });
    }

    function addAuthorizedAccountsTable() {
        var div = document.createElement('div');
        div.className = "panel panel-default";
        div.id = "authorized-accounts";
        div.innerHTML = "<div class='panel-heading'>Your authorized accounts:</div>" +
            "               <div class='panel-body'>" +
            "                   <table class='table table-borderless' id='accounts-table'>" +
            "                   </table>" +
            "                </div>";
        $('#user-details').append(div);

    }

    function addAthorizedRow(account) {
        var tr = document.createElement('tr');
        tr.innerHTML = "<td>Account</td>" +
            "               <td>" +
            "                   <input type='text' class='form-control' id='insta-loged-in' placeholder=" + account + " readonly>" +
            "               </td>" +
            "               <td>" +
            "                   <button type='button' class='btn btn-default' id='btn-insta-logout'>Delete</button>" +
            "               </td>";

        $('#accounts-table').append(tr);
    }

    function unauthorizedRequest(message) {
        var div = document.createElement('div');
        div.id = 'login-exception';
        div.style.color = 'red';
        div.innerHTML = '<p>' + message.message + '</p>';
        $('#instagram-auth-form').append(div);
    }


    $(function () {
        $("#btn-insta-signin").click(function () {
            $('#login-exception').empty();
            signInToInstagram();
        });
    });

    function signInToInstagram() {
        var data = {}
        data["login"] = $("#insta-login").val();
        data["password"] = $("#insta-password").val();
        console.info("Instagram form: " + JSON.stringify(data));
        stompClient.send("/app/instagram-sign-in", {}, JSON.stringify(data));
    }

</script>

<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
