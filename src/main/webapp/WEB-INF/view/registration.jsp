<!DOCTYPE html>

<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/view/header.jsp"/>

<div class="container">
    <h2>Hello ${loginName} please fill in the form below:</h2>
    <form class="form-horizontal" action="/register" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <input type="hidden" value="${loginId}" name="loginId"/>
        <div class="form-group">
            <label class="control-label col-sm-2" for="first_name">First name:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="first_name" placeholder="First name" name="firstName">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="last_name">Last name:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="last_name" placeholder="Last name" name="lastName">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="email" placeholder="E-mail" name="email">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
</div>


<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
