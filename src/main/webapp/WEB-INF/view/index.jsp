<!DOCTYPE html>

<%@ page isELIgnored="false" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/view/header.jsp"/>

<sec:authorize access="isAuthenticated()">
    <div class="container">
        <div class="row">
            <!-- code start -->
            <div class="twPc-div">
                <a class="twPc-bg twPc-block"></a>

                <div>
                    <a title="Mert Salih Kaplan" href="https://twitter.com/mertskaplan" class="twPc-avatarLink">
                        <img alt="Mert Salih Kaplan"
                             src="https://instagram.fwaw3-1.fna.fbcdn.net/t51.2885-19/s150x150/19120512_638509539671106_760484467256066048_n.jpg"
                             class="twPc-avatarImg">
                    </a>

                    <div class="twPc-divUser">
                        <div class="twPc-divName">
                            <a href="https://twitter.com/mertskaplan">Mert S. Kaplan</a>
                        </div>
                        <span>
				<a href="https://twitter.com/mertskaplan">@<span>mertskaplan</span></a>
			</span>
                    </div>

                    <div class="twPc-divStats">
                        <ul class="twPc-Arrange">
                            <li class="twPc-ArrangeSizeFit">
                                <a href="https://www.instagram.com/${username}" title="9.840 Tweet">
                                    <span class="twPc-StatLabel twPc-block">Publications</span>
                                    <span class="twPc-StatValue">${publications}</span>
                                </a>
                            </li>
                            <li class="twPc-ArrangeSizeFit">
                                <a href="https://www.instagram.com/${username}/following/" title="885 Following">
                                    <span class="twPc-StatLabel twPc-block">Following</span>
                                    <span class="twPc-StatValue">${following}</span>
                                </a>
                            </li>
                            <li class="twPc-ArrangeSizeFit">
                                <a href="https://www.instagram.com/${username}/followers/" title="1.810 Followers">
                                    <span class="twPc-StatLabel twPc-block">Followers</span>
                                    <span class="twPc-StatValue">${followers}</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- code end -->
        </div>
    </div>
</sec:authorize>


<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
