<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page isELIgnored="false" %>

<head>
    <title>Intagram ROBO</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/resources/css/modern-business.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/resources/css/profile.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/resources/css/header/login.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/resources/css/header/sign_up.css" rel="stylesheet"/>

    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-social.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet"/>
</head>


<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/index">InstagramROBO</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="${pageContext.request.contextPath}/index">Home</a></li>
            <sec:authorize access="hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Features<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/like-and-follow">Like and follow by tag</a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/command">Explore</a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/command">Unfollow</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configuration<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/user_config">User</a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/insta_config">Instagram</a>
                        </li>
                    </ul>
                </li>
            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li><a href="${pageContext.request.contextPath}/admin">Admin page</a></li>
            </sec:authorize>
        </ul>
        <div class="col-sm-3 col-md-3">
            <form class="navbar-form" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search interesting location" name="q">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <sec:authorize access="isAnonymous()">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                    <ul id="login-dp" class="dropdown-menu">
                        <li>
                            <div class="row">
                                <div class="col-md-12">
                                    Login via
                                    <a href="#" class="btn btn-block btn-social btn-facebook"><span
                                            class="fa fa-facebook"></span>Facebook</a>
                                    <a href="#" class="btn btn-block btn-social btn-instagram"><span
                                            class="fa fa-instagram"></span>Instagram</a>
                                    or
                                    <form class="form" role="form" method="post" action="/j_login"
                                          accept-charset="UTF-8"
                                          id="login-nav">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                            <input type="email" name="j_username" class="form-control"
                                                   id="exampleInputEmail2"
                                                   placeholder="Email address" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputPassword2">Password</label>
                                            <input type="password" name="j_password" class="form-control"
                                                   id="exampleInputPassword2"
                                                   placeholder="Password" required>
                                            <div class="help-block text-right"><a href="">Forget the password ?</a>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember-me"> keep me logged-in
                                            </label>
                                        </div>
                                    </form>
                                </div>
                                <div class="bottom text-center">
                                    New here ? <a href="#signup-box" class="signup-window"><b>Sign Up</b></a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>

        </sec:authorize>
        <sec:authorize access="isAuthenticated()">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="/logout" class="dropdown-toggle"><b>Logout</b></a>
                </li>
            </ul>
        </sec:authorize>
    </div>
</nav>

<sec:authorize access="isAnonymous()">
<div id="signup-box" class="signup-popup">
    <button type="button" class="close" id="btn_close" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <form method="post" class="signin" action="/sign-up">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <fieldset class="textbox">
            <label class="username">
                <span>Username or email</span>
                <input id="username" name="username" value="" type="text" autocomplete="on" placeholder="Username">
            </label>
            <label class="password">
                <span>Password</span>
                <input id="password" name="password" value="" type="password" placeholder="Password">
            </label>
            <label class="password">
                <span>Repeat password</span>
                <input id="re-password" name="password" value="" type="password" placeholder="Password">
            </label>
            <button class="submit button" type="submit">Sign up</button>
        </fieldset>
    </form>
</div>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('a.signup-window').click(function () {

            // Getting the variable's value from a link
            var loginBox = $(this).attr('href');

            //Fade in the Popup and add close button
            $(loginBox).fadeIn(300);

            //Set the center alignment padding + border
            var popMargTop = ($(loginBox).height() + 24) / 2;
            var popMargLeft = ($(loginBox).width() + 24) / 2;

            $(loginBox).css({
                'margin-top': -popMargTop,
                'margin-left': -popMargLeft
            });

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return false;
        });

        // When clicking on the button close or the mask layer the popup closed
        $('#signup-box').on('click', '.close', function () {
            $('#mask , .signup-popup').fadeOut(300, function () {
                $('#mask').remove();
            });
            return false;
        });
    });
</script>
</sec:authorize>
