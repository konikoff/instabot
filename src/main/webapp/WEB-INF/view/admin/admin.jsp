<!DOCTYPE html>

<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/view/header.jsp"/>
<link href="${pageContext.request.contextPath}/resources/css/scroll/scroll.css" rel="stylesheet"/>

<div class="container">
    <h2>User Statistic</h2>
    <p>This page show statistic about users:</p>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Login</th>
            <th>First name</th>
            <th>Last name</th>
            <th>E-mail</th>
            <th>Instagram account</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${userInfo}" var="users">
            <tr>
                <td>${users.login}</td>
                <td>${users.firstName}</td>
                <td>${users.lastName}</td>
                <td>${users.eMail}</td>
                <td>${users.instagramAccount}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <ul class="pagination">
        <li><a href="#">1</a></li>
        <li class="active"><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
    </ul>
</div>

</body>
</html>
