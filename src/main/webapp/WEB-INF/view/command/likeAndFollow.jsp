<!DOCTYPE html>

<%@ page isELIgnored="false" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/view/header.jsp"/>

<link href="${pageContext.request.contextPath}/resources/css/scroll/scroll.css" rel="stylesheet"/>
<div class="panel panel-default">
    <!-- Default panel contents -->
    <table class="table table-borderless" id="instagram-account">
        <tr>
            <td class="panel-heading">Instagram account:</td>
            <td>
                <select id="select-account" name="select-account" class="form-control">
                    <c:forEach items="${instagram_accounts}" var="account">
                        <option value="${account}">${account}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
    </table>

    <div class="panel-body">
        <div class="col-xs-5" id="user-details">
            <div class="panel panel-default">
                <div class="panel-heading">Action:</div>
                <div class="panel-body">
                    <table class="table table-borderless" id="instagram-auth-form">
                        <tr class="instagram-action-row">
                            <td>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                                <span class="input-group-addon">
                                                    <span>Like
                                                        <input type="radio" name="action-group" value="like">
                                                    </span>
                                                    <span>Follow
                                                        <input type="radio" name="action-group" value="follow">
                                                    </span>
                                                </span>
                                            <input type="text" name="action-tag" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default instagram-start" type="button">
                                                    <span class="glyphicon glyphicon-play"/></button>
                                                <button class="btn btn-default instagram-stop" type="button">
                                                    <span class="glyphicon glyphicon-stop"/>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="${pageContext.request.contextPath}/resources/js/jquery.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.serializeObject.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/stomp/sockjs-0.3.4.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/stomp/stomp.js" type="text/javascript"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

<script type="text/javascript">
    window.onload = connect;

    var stompClient = null;

    function connect() {
        var socket = new SockJS('/web-service');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/topic/instabot', function (greeting) {
                setValue(greeting.body);
                //showGreeting(JSON.parse(greeting.body).content);
            });
            stompClient.subscribe('/user/queue/like-follow', function (likeFollow) {
                updateLikeFollow(likeFollow.body);
            });
        });
    }

    function setValue(message) {
        $(function () {
            console.log(message);
            $("#action").text("http://help.ru");
            $("#image_source").attr("src", "https://i.stack.imgur.com/VeMux.jpg");
        })
    }

    function updateLikeFollow(message) {
        $(function () {
            var likeFollwoObj = JSON.parse(message);
            $("#image_source").attr("src", likeFollwoObj.object.displayUrl);
            $("#action").text(likeFollwoObj.status);
            $("#instagram_image_url").attr("href", "https://www.instagram.com/p/" + likeFollwoObj.object.shortCode);
            $("#intsagram_image_description").text(likeFollwoObj.object.description);
            console.log(message);
        })
    }

    $(function () {
        $("#send").click(function () {
            sendName();
        });
    });

    function sendName() {
        var formData = JSON.stringify($("#tagForm").serializeObject());
        stompClient.send("/app/execute_command", {}, formData);
    }

    function startAction(action) {
        stompClient.send("/app/execute_command", {}, JSON.stringify(action));
    }

    function stopAction() {
        var action = new Object();
        action.stop = true;
        action.action = "";
        action.tag = "";

        var instaAccount = document.getElementById('select-account');
        action.account = instaAccount.options[instaAccount.selectedIndex].text;

        stompClient.send("/app/execute_command", {}, JSON.stringify(action));
    }

    $(function () {
        $('.instagram-start').click(function () {
            var actionObj = new Object();
            var actionGroup = document.getElementsByName('action-group');
            var i;
            for (i = 0; i < actionGroup.length; i++) {
                if (actionGroup[i].checked) {
                    actionObj.action = actionGroup[i].value;
                }
            }

            var instaAccount = document.getElementById('select-account');
            actionObj.account = instaAccount.options[instaAccount.selectedIndex].text;


            var actionTags = document.getElementsByName('action-tag');
            actionTags.forEach(function (value) {
                actionObj.tag = value.value;
            });

            actionObj.stop = false;
            startAction(actionObj);

        });

        $('.instagram-stop').click(function () {
            stopAction();
        });

    });
</script>

</body>
</html>
