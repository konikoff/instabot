<!DOCTYPE html>

<%@ page isELIgnored="false" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/view/header.jsp"/>

<link href="${pageContext.request.contextPath}/resources/css/scroll/scroll.css" rel="stylesheet"/>
<div class="panel panel-default">
    <!-- Default panel contents -->
    <table class="table table-borderless" id="instagram-account">
        <tr>
            <td class="panel-heading">Instagram account:</td>
            <td>
                <select id="select-account" name="action" class="form-control">
                    <option value="account_1">account 1</option>
                    <option value="account_2">account 2</option>
                </select>
            </td>
        </tr>
    </table>

    <%--<div class="panel-heading">Instagram Account: </div>--%>
    <div class="panel-body">
        <div class="col-xs-5" id="user-details">
            <div class="panel panel-default">
                <div class="panel-heading">Instagram authorization:</div>
                <div class="panel-body">
                    <table class="table table-borderless" id="instagram-auth-form">
                        <tr>
                            <td>Login</td>
                            <td>
                                <input type="text" class="form-control" placeholder="Login" id="insta-login">
                            </td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td>
                                <input type="text" class="form-control" placeholder="Password" id="insta-password">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-default" id="btn-insta-signin">Sign in</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row col-xs-12 col-sm-6 col-md-8">
        <form class="form-inline" id="tagForm">
            <h3>Tags</h3>
            <div class="row col-sm-4">
                <select id="insta_action" name="action" class="form-control">
                    <option value="follow">Follow & Like</option>
                    <option value="like">Like</option>
                </select>
                <button type="button" id="send" class="btn btn-default">Start</button>
            </div>

            <div class="row">
                <div class="form-group form-group-options col-xs-11 col-sm-4 col-md-4">
                    <div class="input-group input-group-option col-xs-12">
                        <input type="text" name="option[]" class="form-control" placeholder="Insert your tag"/>
                        <span class="input-group-addon input-group-addon-remove">
    				<span class="glyphicon glyphicon-remove"></span>
    			</span>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="row col-xs-6 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" id="action">Like or Follow</h3>
            </div>
            <div class="panel-body">
                <a target="_blank" href="#" class="twPc-avatarLink" id="instagram_image_url">
                    <img class="twPc-avatarImg" src="#" id="image_source">
                </a>
                <div id="intsagram_image_description"></div>
            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/resources/js/jquery.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.serializeObject.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/stomp/sockjs-0.3.4.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/stomp/stomp.js" type="text/javascript"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(function () {

        $(document).on('focus', 'div.form-group-options div.input-group-option:last-child input', function () {

            var sInputGroupHtml = $(this).parent().html();
            var sInputGroupClasses = $(this).parent().attr('class');
            $(this).parent().parent().append('<div class="' + sInputGroupClasses + '">' + sInputGroupHtml + '</div>');

        });

        $(document).on('click', 'div.form-group-options .input-group-addon-remove', function () {

            $(this).parent().remove();

        });

    });

    /*
        Selects
    */
    $(function () {

        var values = new Array();

        $(document).on('change', '.form-group-multiple-selects .input-group-multiple-select:last-child select', function () {

            var selectsLength = $(this).parent().parent().find('.input-group-multiple-select select').length;
            var optionsLength = ($(this).find('option').length) - 1;

            if (selectsLength < optionsLength) {
                var sInputGroupHtml = $(this).parent().html();
                var sInputGroupClasses = $(this).parent().attr('class');
                $(this).parent().parent().append('<div class="' + sInputGroupClasses + '">' + sInputGroupHtml + '</div>');
            }

            updateValues($(this).parent().parent());

        });

        $(document).on('change', '.form-group-multiple-selects .input-group-multiple-select:not(:last-child) select', function () {

            updateValues($(this).parent().parent());

        });

        $(document).on('click', '.input-group-addon-remove', function () {

            var oSelectContainer = $(this).parent().parent()
            $(this).parent().remove();
            updateValues(oSelectContainer);

        });

        function updateValues(oSelectContainer) {

            values = new Array();
            $(oSelectContainer).find('.input-group-multiple-select select').each(function () {
                var value = $(this).val();
                if (value != 0 && value != "") {
                    values.push(value);
                }
            });

            $(oSelectContainer).find('.input-group-multiple-select select').find('option').each(function () {
                var optionValue = $(this).val();
                var selectValue = $(this).parent().val();
                if (in_array(optionValue, values) != -1 && selectValue != optionValue) {
                    $(this).attr('disabled', 'disabled');
                }
                else {
                    $(this).removeAttr('disabled');
                }
            });

        }

        function in_array(needle, haystack) {

            var found = 0;
            for (var i = 0, length = haystack.length; i < length; i++) {
                if (haystack[i] == needle) return i;
                found++;
            }
            return -1;

        }

        // Update all options for first use
        $('.form-group-multiple-selects').each(function (i, e) {

            updateValues(e);

        });
    });
</script>

<script type="text/javascript">
    window.onload = connect;

    var stompClient = null;

    function connect() {
        var socket = new SockJS('/web-service');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/topic/instabot', function (greeting) {
                setValue(greeting.body);
                //showGreeting(JSON.parse(greeting.body).content);
            });
            stompClient.subscribe('/user/queue/like-follow', function (likeFollow) {
                updateLikeFollow(likeFollow.body);
            });
        });
    }

    function setValue(message) {
        $(function () {
            console.log(message);
            $("#action").text("http://help.ru");
            $("#image_source").attr("src", "https://i.stack.imgur.com/VeMux.jpg");
        })
    }

    function updateLikeFollow(message) {
        $(function () {
            var likeFollwoObj = JSON.parse(message);
            $("#image_source").attr("src", likeFollwoObj.object.displayUrl);
            $("#action").text(likeFollwoObj.status);
            $("#instagram_image_url").attr("href", "https://www.instagram.com/p/" + likeFollwoObj.object.shortCode);
            $("#intsagram_image_description").text(likeFollwoObj.object.description);
            console.log(message);
        })
    }

    $(function () {
        $("#send").click(function () {
            sendName();
        });
    });

    function sendName() {
        var formData = JSON.stringify($("#tagForm").serializeObject());
        stompClient.send("/app/execute_command", {}, formData);
    }

    function showGreeting(message) {
        alert(message);
    }
</script>

</body>
</html>
