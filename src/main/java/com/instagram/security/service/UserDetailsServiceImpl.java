package com.instagram.security.service;

import com.instagram.db.Credentials;
import com.instagram.db.UserRoles;
import com.instagram.db.service.CredentialsService;
import com.instagram.db.service.UserRolesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private CredentialsService credentialsService;
    private UserRolesService userRolesService;

    @Autowired
    @Qualifier(value = "credentialsService")
    public void setCredentialsService(CredentialsService credentialsService) {
        this.credentialsService = credentialsService;
    }

    @Autowired
    @Qualifier(value = "userRolesService")
    public void setUserRolesService(UserRolesService userRolesService) {
        this.userRolesService = userRolesService;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        Set<GrantedAuthority> roles = new HashSet();

        Credentials userCredentials = credentialsService.getCredentialsByLogin(userName);
        logger.debug("UserCredentials: " + userCredentials);
        UserRoles userRole = userRolesService.getRoleForSpecificUser(userCredentials);
        logger.debug("User role: " + userRole);
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(userRole.getRole().getRole());
        roles.add(simpleGrantedAuthority);


        return new org.springframework.security.core.userdetails.User(userCredentials.getLogin(),
                userCredentials.getPassword(),
                roles);
    }
}
