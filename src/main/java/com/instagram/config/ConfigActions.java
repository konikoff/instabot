package com.instagram.config;


import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class ConfigActions {
    private List<ConfigAction> actions = new ArrayList<>();

    @XmlElement(name = "action")
    public List<ConfigAction> getActions() {
        return actions;
    }

    public void setActions(List<ConfigAction> actions) {
        this.actions = actions;
    }

}
