package com.instagram.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Configuration {
    private String login;
    private String password;
    private ConfigActions configActions;

    @XmlElement
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @XmlElement
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlElement(name = "actions")
    public ConfigActions getConfigActions() {
        return configActions;
    }

    public void setConfigActions(ConfigActions configActions) {
        this.configActions = configActions;
    }
}
