package com.instagram.config;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class Test {
    public static void main(String[] a) {
        Configuration configuration = new Configuration();
        List<ConfigAction> actionList = new ArrayList<>();
        actionList.add(new ConfigAction() {{
            setAction("like1");
            setTag("paris");
        }});
        actionList.add(new ConfigAction() {{
            setAction("like2");
            setTag("paris");
        }});
        actionList.add(new ConfigAction() {{
            setAction("like3");
            setTag("paris");
        }});

        configuration.setLogin("loginA");
        configuration.setPassword("PasswordA");
        configuration.setConfigActions(new ConfigActions() {{
            setActions(actionList);
        }});

        convertObjectToXml(configuration);
    }

    // восстанавливаем объект из XML файла
    private static Configuration fromXmlToObject(String filePath) {
        try {
            // создаем объект JAXBContext - точку входа для JAXB
            JAXBContext jaxbContext = JAXBContext.newInstance(Configuration.class);
            Unmarshaller un = jaxbContext.createUnmarshaller();

            return (Configuration) un.unmarshal(new File(filePath));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    // сохраняем объект в XML файл
    private static void convertObjectToXml(Configuration configuration) {
        try {
            JAXBContext context = JAXBContext.newInstance(Configuration.class);
            Marshaller marshaller = context.createMarshaller();
            // устанавливаем флаг для читабельного вывода XML в JAXB
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            // маршаллинг объекта в файл
            marshaller.marshal(configuration, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
