package com.instagram.web.controller;

import com.instagram.db.Credentials;
import com.instagram.db.Roles;
import com.instagram.db.User;
import com.instagram.db.UserRoles;
import com.instagram.db.service.RolesService;
import com.instagram.db.service.UserRolesService;
import com.instagram.db.service.UserService;
import com.instagram.web.model.json.MultiTagWrapper;
import com.instagram.db.service.CredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@Scope("session")
public class MainController {

    private boolean tempFlag = false;

    @Autowired
    private CredentialsService credentialsService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRolesService userRolesService;

    @Autowired
    private RolesService rolesService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String viewHome() {
        return "home";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String viewIndex() {
        tempCreatingUser();
        return "index";
    }

    @RequestMapping(value = "/command", method = RequestMethod.GET)
    public String viewCommand() {
        return "command/commands";
    }

    @RequestMapping(value = "/command", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MultiTagWrapper> getTags(@RequestBody MultiTagWrapper data) {
        MultiTagWrapper tags = data;
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/index";
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String signUpPage(@RequestBody MultiValueMap<String, String> formData, ModelMap modelMap) {
        Credentials newCredentials = new Credentials();

        newCredentials.setLogin(String.valueOf(formData.get("username").get(0)));
        newCredentials.setPassword(passwordEncoder.encode(String.valueOf(formData.get("password").get(0))));
        long newUserId = credentialsService.createNewAccount(newCredentials);

        modelMap.addAttribute("loginId", newUserId);
        modelMap.addAttribute("loginName", newCredentials.getLogin());
        return "registration";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String registerForm(@RequestBody MultiValueMap<String, String> registerFormData) {
        User newUser = new User();
        newUser.setFirstname(String.valueOf(registerFormData.get("firstName").get(0)));
        newUser.setLastname(String.valueOf(registerFormData.get("lastName").get(0)));
        newUser.setLoginId(String.valueOf(registerFormData.get("loginId").get(0)));
        newUser.setEmail(String.valueOf(registerFormData.get("email").get(0)));

        userService.createNewUser(newUser);

        UserRoles userRole = new UserRoles();
        Credentials userCredentials = credentialsService.getCredentialsById(Integer.parseInt(newUser.getLoginId()));
        userRole.setCredentials(userCredentials);
        userRole.setRole(rolesService.getRoleByName("ROLE_ADMIN"));

        userRolesService.assignRoleToUser(userRole);

        return "redirect:/index";
    }

    /**
     * Delete this method after development
     */
    private void tempCreatingUser() {
        if (!tempFlag) {
            Credentials newCredentials = new Credentials();
            newCredentials.setLogin("admin@admin.ua");
            newCredentials.setPassword(passwordEncoder.encode("admin"));
            credentialsService.createNewAccount(newCredentials);

            Roles roles = new Roles();
            roles.setRole("ROLE_ADMIN");
            rolesService.createNewRole(roles);

            UserRoles userRole = new UserRoles();
            userRole.setCredentials(newCredentials);
            userRole.setRole(roles);
            userRolesService.assignRoleToUser(userRole);
            this.tempFlag = true;
        }
    }
}
