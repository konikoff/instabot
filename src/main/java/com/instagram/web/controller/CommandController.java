package com.instagram.web.controller;

import com.instagram.db.Credentials;
import com.instagram.db.service.CredentialsService;
import com.instagram.db.service.InstagramAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.LinkedList;
import java.util.List;

@Controller
@Scope("session")
public class CommandController {

    @Autowired
    private CredentialsService credentialsService;

    @Autowired
    private InstagramAccountService instagramAccountService;

    @RequestMapping(value = "/like-and-follow", method = RequestMethod.GET)
    public String followCommand(Principal principal, Model model) {
        Credentials credentials = credentialsService.getCredentialsByLogin(principal.getName());

        List<String> instagramAccountList = new LinkedList<>();
        instagramAccountService.getAccountsBelongToUser(credentials).forEach(account -> instagramAccountList.add(account.getLoginName()));
        model.addAttribute("instagram_accounts", instagramAccountList);
        return "command/likeAndFollow";
    }
}
