package com.instagram.web.controller;

import com.instagram.web.model.admin.UserInfo;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@Scope("session")
public class AdminController {

    @RequestMapping(value = "/admin", method = GET)
    public String viewHome() {
        return "admin/admin";
    }

    @RequestMapping(value = "admin/user_info", params = {"page", "size"}, method = GET)
    @ResponseBody
    public List<UserInfo> getUserInfo(@RequestParam("page") int page, @RequestParam("size") int size, UriComponentsBuilder uriBuilder, HttpServletResponse response) {
        List<UserInfo> userInfos = new ArrayList<>();
        return userInfos;
    }
}
