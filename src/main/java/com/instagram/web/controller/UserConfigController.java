package com.instagram.web.controller;

import com.instagram.db.Credentials;
import com.instagram.db.InstagramAccounts;
import com.instagram.db.service.CredentialsService;
import com.instagram.db.service.InstagramAccountService;
import com.instagram.db.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@Scope("session")
public class UserConfigController {

    @Autowired
    private CredentialsService credentialsService;

    @Autowired
    private UserService userService;

    @Autowired
    private InstagramAccountService instagramAccountService;

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @RequestMapping(value = "/user_config", method = RequestMethod.GET)
    public String viewUserConfig(ModelMap modelMap) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        modelMap.addAttribute("user_login", user.getUsername());

        Credentials credentials = credentialsService.getCredentialsByLogin(user.getUsername());

        modelMap.addAttribute("user_name", credentials.getUser().getFirstname());
        modelMap.addAttribute("user_surname", credentials.getUser().getLastname());
        modelMap.addAttribute("user_email", credentials.getUser().getEmail());

        return "config/user_config";
    }

    @RequestMapping(value = "/insta_config", method = RequestMethod.GET)
    public String viewInstaConfig() {
        return "config/insta_config";
    }

    @RequestMapping(value = "/instagramAuthorizedAccounts", method = RequestMethod.GET)
    public void getInstagramAutorizedAccounts(Principal principal) {
        Credentials credentials = credentialsService.getCredentialsByLogin(principal.getName());
        List<String> users = new LinkedList<>();
        credentials.getInstagramAccounts().stream().filter(Objects::nonNull).forEach(user -> users.add(user.getLoginName()));

        Map<String, Object> map = new HashMap<>();
        map.put(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON);
        messagingTemplate.convertAndSendToUser(principal.getName(), "/queue/instagram/authorizedAccounts", users, map);
    }
}
