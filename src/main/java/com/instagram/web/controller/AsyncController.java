package com.instagram.web.controller;

import com.instagram.action.LikeCommand;
import com.instagram.action.service.ActionService;
import com.instagram.action.service.ExecuteService;
import com.instagram.auth.AuthorizationStatus;
import com.instagram.auth.service.InstagramAuthentication;
import com.instagram.db.Credentials;
import com.instagram.db.InstagramAccounts;
import com.instagram.db.service.CredentialsService;
import com.instagram.db.service.InstagramAccountService;
import com.instagram.web.model.instagram.InstagramAction;
import com.instagram.web.model.instagram.InstagramForm;
import com.instagram.web.model.notification.ExecutingStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextListener;

import javax.servlet.annotation.WebListener;
import java.security.Principal;

@Controller
@WebListener
public class AsyncController extends RequestContextListener {

    @Autowired
    private InstagramAuthentication authentication;

    @Autowired
    private InstagramAccountService instagramAccountService;

    @Autowired
    private CredentialsService credentialsService;

    @Autowired
    private ActionService actionService;

    @MessageMapping("/execute_command")
    @SendTo("/topic/instabot")
    public ExecutingStep followAndLikeByTag(InstagramAction instagramAction, Principal principal) {
        String user = principal.getName();
        Credentials credentials = credentialsService.getCredentialsByLogin(user);

        InstagramAccounts instagramAccount = instagramAccountService.getAccountsBelongToUser(credentials).stream().filter(account -> instagramAction.getAccount().equals(account.getLoginName())).findFirst().get();
        switch (instagramAction.getAction()) {
            case "like":
                actionService.like(instagramAccount);
                break;
            case "follow":
                actionService.follow(instagramAccount);
                break;
        }

        return new ExecutingStep("You " + instagramAction.getAction() + "!");
    }

    @MessageMapping("/instagram-sign-in")
    @SendTo("/topic/instagram/auth")
    public AuthorizationStatus instaSignIn(InstagramForm signInForm, Principal principal) {
        System.out.println(signInForm);
        /**
         * 1. Save instagram credentials
         * 2. Authorize on instagram.com
         */

        return authentication.login(signInForm, principal);
    }
}
