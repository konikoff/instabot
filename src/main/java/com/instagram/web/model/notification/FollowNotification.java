package com.instagram.web.model.notification;

public class FollowNotification {
    private String action;
    private String imageSource;
    private int currentPointer;
    private int maxOnPage;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public int getCurrentPointer() {
        return currentPointer;
    }

    public void setCurrentPointer(int currentPointer) {
        this.currentPointer = currentPointer;
    }

    public int getMaxOnPage() {
        return maxOnPage;
    }

    public void setMaxOnPage(int maxOnPage) {
        this.maxOnPage = maxOnPage;
    }
}
