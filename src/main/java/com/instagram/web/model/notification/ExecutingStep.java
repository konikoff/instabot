package com.instagram.web.model.notification;

public class ExecutingStep {
    private String step;
    private String describe;

    public ExecutingStep(String describe) {
        this.describe = describe;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }
}
