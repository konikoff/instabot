package com.instagram.web.model.instagram;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonPropertyOrder({"action", "tag", "stop", "account"})
public class InstagramAction {

    private String action;
    private String tag;
    private boolean stop;
    private String account;

    public String getAction() {
        return action;
    }

    @JsonSetter("action")
    public void setAction(String action) {
        this.action = action;
    }

    @JsonGetter("tag")
    public String getTag() {
        return tag;
    }

    @JsonGetter("tag")
    public void setTag(String tag) {
        this.tag = tag;
    }

    @JsonGetter("stop")
    public boolean isStop() {
        return stop;
    }

    @JsonSetter("stop")
    public void setStop(boolean stop) {
        this.stop = stop;
    }

    @JsonGetter("account")
    public String getAccount() {
        return account;
    }

    @JsonSetter("account")
    public void setAccount(String account) {
        this.account = account;
    }
}
