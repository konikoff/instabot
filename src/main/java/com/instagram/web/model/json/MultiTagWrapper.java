package com.instagram.web.model.json;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.List;

@JsonPropertyOrder({"action", "option[]"})
public class MultiTagWrapper implements Serializable {

    private String action;
    private List<String> option;

    public MultiTagWrapper() {
    }

    @JsonGetter("action")
    public String getAction() {
        return action;
    }

    @JsonSetter("action")
    public void setAction(String action) {
        this.action = action;
    }

    @JsonGetter("option[]")
    public List<String> getOption() {
        return option;
    }

    @JsonSetter("option[]")
    public void setOption(List<String> option) {
        this.option = option;
    }
}
