package com.instagram.web.model.action;

public class CommandActions {
    private String action;

    public CommandActions() {
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
