package com.instagram.web.service;

import java.util.List;

@Deprecated
public interface FollowLikeService {
    void executeFollowLike(List<String> tags, String action, String user);
}
