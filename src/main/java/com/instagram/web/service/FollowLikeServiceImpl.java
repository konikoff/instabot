package com.instagram.web.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Deprecated
@Service
public class FollowLikeServiceImpl implements FollowLikeService {
    private static final Log logger = LogFactory.getLog(FollowLikeServiceImpl.class);

    private final SimpMessageSendingOperations messagingTemplate;
    private final static int THREAD_COUNT = 5;
    private String user;


    @Autowired
    public FollowLikeServiceImpl(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Override
    public void executeFollowLike(List<String> tags, String action, String user) {
        boolean isFollow = action.equals("follow");
        this.user = user;
        ExecutorService execService = Executors.newFixedThreadPool(THREAD_COUNT);
        List<Future<String>> futures = new ArrayList<>();
        for (String tag : tags) {
            if (!tag.isEmpty()) {
               // LikeAndFollow likeAndFollow = new LikeAndFollow(authService.getAuth(), tag, isFollow, this);
               // futures.add(execService.submit(likeAndFollow));
            }
        }
        execService.shutdown();
    }


    public void actionPerform(Object o) {
        Map<String, Object> map = new HashMap<>();
        map.put(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON);
        logger.debug("Perform callback action: " + o);
        this.messagingTemplate.convertAndSendToUser(user, "/queue/like-follow", o, map);
    }
}
