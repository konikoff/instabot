package com.instagram.exception;

/**
 * Created by anton on 10/16/2017.
 */
public class AuthenticationException extends Exception {

    public AuthenticationException() {
        super("Authentication Exception, please check your login and password");
    }
}
