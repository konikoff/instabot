package com.instagram.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.Cookie;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Set;

public class Utilities {
    private static final Log logger = LogFactory.getLog(Utilities.class);

    public static Set<Cookie> bytesToCookies(byte[] blob) {
        Set<Cookie> cookieSet = null;
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(blob);
             ObjectInputStream ois = new ObjectInputStream(byteArrayInputStream)) {

            cookieSet = (HashSet<Cookie>) ois.readObject();

        } catch (IOException | ClassNotFoundException e) {
            logger.error("Parsing exception.", e);
        }
        return cookieSet;

    }
}
