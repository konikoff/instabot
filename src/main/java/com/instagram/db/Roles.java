package com.instagram.db;

import javax.persistence.*;

@Table(name = "ROLES")
@Entity
public class Roles {

    @Id
    @Column(name = "ROLE_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long roleId;

    @Column(name = "ROLE", nullable = false, unique = true)
    private String role;

    @OneToOne(mappedBy = "role")
    private UserRoles userRole;

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public UserRoles getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRoles userRole) {
        this.userRole = userRole;
    }
}
