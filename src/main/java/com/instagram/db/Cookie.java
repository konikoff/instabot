package com.instagram.db;

import javax.persistence.*;

@Table(name = "COOKIE")
@Entity
public class Cookie {

    @Id
    @Column(name = "cookie_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "COOKIES")
    @Lob
    private byte[] cookies;

    @OneToOne
    @JoinColumn(name = "inst_id", nullable = false)
    private InstagramAccounts instagramAccounts;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getCookies() {
        return cookies;
    }

    public void setCookies(byte[] cookies) {
        this.cookies = cookies;
    }

    public InstagramAccounts getInstagramAccounts() {
        return instagramAccounts;
    }

    public void setInstagramAccounts(InstagramAccounts instagramAccounts) {
        this.instagramAccounts = instagramAccounts;
    }
}
