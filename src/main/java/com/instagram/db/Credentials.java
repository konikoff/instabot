package com.instagram.db;

import javax.persistence.*;
import java.util.Set;

@Table(name = "CREDENTIALS")
@Entity
public class Credentials {

    @Id
    @Column(name = "credential_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "LOGIN", unique = true, nullable = false)
    private String login;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @OneToOne(mappedBy = "credentials")
    private UserRoles userRole;

    @OneToOne
    @JoinColumn(name = "id")
    private User user;

    @OneToMany(mappedBy = "credentials", fetch = FetchType.EAGER)
    private Set<InstagramAccounts> instagramAccounts;

    public Set<InstagramAccounts> getInstagramAccounts() {
        return instagramAccounts;
    }

    public void setInstagramAccounts(Set<InstagramAccounts> instagramAccounts) {
        this.instagramAccounts = instagramAccounts;
    }

    public UserRoles getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRoles userRole) {
        this.userRole = userRole;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
