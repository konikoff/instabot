package com.instagram.db.dao;

import com.instagram.db.User;

public interface UserDAO {

    User getUserById(long id);

    long createNewUser(User newUser);

    void deleteUserById(long id);

    void updateUserData(User user);
}
