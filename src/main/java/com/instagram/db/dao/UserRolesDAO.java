package com.instagram.db.dao;

import com.instagram.db.Credentials;
import com.instagram.db.UserRoles;

public interface UserRolesDAO {
    UserRoles getRoleForSpecificUser(Credentials credentials);

    long assignRoleToUser(UserRoles userRole);

    void deleteRoleFromUserById(long id);

    void updateUserRole(UserRoles userRole);
}
