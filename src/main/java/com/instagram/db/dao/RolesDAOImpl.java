package com.instagram.db.dao;

import com.instagram.db.Roles;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RolesDAOImpl implements RolesDAO {
    private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

    private SessionFactory sessionFactory;

    public RolesDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Roles getRoleById(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        Roles role = session.get(Roles.class, id);
        return role;
    }

    @Override
    public Roles getRoleByName(String roleName) {
        Session session = this.sessionFactory.getCurrentSession();
        Query queryGetRole = session.createQuery("from Roles where role=:role");
        queryGetRole.setParameter("role", roleName);

        return (Roles) queryGetRole.uniqueResult();
    }

    @Override
    public long createNewRole(Roles role) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(role);
        logger.debug("Role added. Role: " + role.toString());
        return role.getRoleId();
    }

    @Override
    public void deleteRoleById(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        Roles deleteRole = session.load(Roles.class, id);
        session.delete(deleteRole);
        session.flush();
        logger.debug("Role deleted. Role: " + deleteRole.toString());

    }

    @Override
    public void updateRoleData(Roles role) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(role);
        logger.debug("Role updated. Role: " + role.toString());
    }
}
