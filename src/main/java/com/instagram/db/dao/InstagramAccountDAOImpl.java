package com.instagram.db.dao;

import com.instagram.db.Credentials;
import com.instagram.db.InstagramAccounts;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class InstagramAccountDAOImpl implements InstagramAccountDAO {
    private static final Logger logger = LoggerFactory.getLogger(InstagramAccountDAOImpl.class);
    private SessionFactory sessionFactory;

    public InstagramAccountDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveOrUpdateAccount(InstagramAccounts accounts) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(accounts);
    }

    @Override
    public InstagramAccounts getAccount(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.load(InstagramAccounts.class, id);
    }

    @Override
    public InstagramAccounts getAccountByLogin(String login) {
        Session session = this.sessionFactory.getCurrentSession();
        Query getAccountByLoginQuery = session.createQuery("FROM InstagramAccounts WHERE loginName=:login");
        getAccountByLoginQuery.setParameter("login", login);
        return (InstagramAccounts) getAccountByLoginQuery.uniqueResult();
    }

    @Override
    public void deleteAccount(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        InstagramAccounts account = session.load(InstagramAccounts.class, id);
        session.delete(account);
        session.flush();
        logger.debug("Instagram account deleted. Instagram: " + account.toString());
    }

    @Override
    public List<InstagramAccounts> getAccountsBelongToUser(Credentials credentials) {
        Session session = this.sessionFactory.getCurrentSession();
        Query getAccountBelongToCredentialQuery = session.createQuery("FROM InstagramAccounts WHERE credentials=:credentials");
        getAccountBelongToCredentialQuery.setParameter("credentials", credentials);

        return (List<InstagramAccounts>) getAccountBelongToCredentialQuery.list();
    }
}
