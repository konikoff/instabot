package com.instagram.db.dao;

import com.instagram.db.Cookie;
import com.instagram.db.InstagramAccounts;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class CookieDAOImpl implements CookieDAO {

    private static final Logger logger = LoggerFactory.getLogger(CookieDAOImpl.class);

    private SessionFactory sessionFactory;

    public CookieDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void saveCookies(Cookie cookie) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(cookie);
    }

    @Override
    public void deleteCookies(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Cookie cookie = session.load(Cookie.class, id);
        session.delete(cookie);
        session.flush();
        logger.debug("Cookie deleted. Cookie: " + cookie.toString());
    }

    @Override
    public Cookie getCookies(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Cookie cookie = session.load(Cookie.class, id);
        return cookie;
    }

    @Override
    public Cookie getCookiesByInstaAccount(InstagramAccounts account) {
        Session session = this.sessionFactory.getCurrentSession();
        Query getCookieByInstaAccountQuery = session.createQuery("FROM Cookie WHERE instagramAccounts=:account");
        getCookieByInstaAccountQuery.setParameter("account", account);
        return (Cookie) getCookieByInstaAccountQuery.uniqueResult();
    }
}
