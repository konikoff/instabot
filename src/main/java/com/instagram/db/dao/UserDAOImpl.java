package com.instagram.db.dao;

import com.instagram.db.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl implements UserDAO {

    private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

    private SessionFactory sessionFactory;

    public UserDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User getUserById(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        User user = session.get(User.class, id);
        return user;
    }

    @Override
    public long createNewUser(User newUser) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(newUser);
        logger.debug("User added. User: " + newUser.toString());
        return newUser.getId();
    }

    @Override
    public void deleteUserById(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        User deleteUser = session.load(User.class, id);
        session.delete(deleteUser);
        session.flush();
        logger.debug("User deleted. User: " + deleteUser.toString());
    }

    @Override
    public void updateUserData(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(user);
        logger.debug("Updated user data. User: " + user.toString());
    }
}
