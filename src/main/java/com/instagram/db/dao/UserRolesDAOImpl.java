package com.instagram.db.dao;

import com.instagram.db.Credentials;
import com.instagram.db.UserRoles;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class UserRolesDAOImpl implements UserRolesDAO {

    private static final Logger logger = LoggerFactory.getLogger(UserRolesDAOImpl.class);

    private SessionFactory sessionFactory;

    public UserRolesDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserRoles getRoleForSpecificUser(Credentials credentials) {
        Session session = this.sessionFactory.getCurrentSession();

        Query queryGetRole = session.createQuery("from UserRoles where credentials=:credentials");
        queryGetRole.setParameter("credentials", credentials);

        UserRoles userRole = (UserRoles) queryGetRole.uniqueResult();
        return userRole;
    }

    @Override
    public long assignRoleToUser(UserRoles userRole) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(userRole);
        session.flush();
        return userRole.getUserRoleId();
    }

    @Override
    public void deleteRoleFromUserById(long id) {

    }

    @Override
    public void updateUserRole(UserRoles userRole) {

    }
}
