package com.instagram.db.dao;

import com.instagram.db.Credentials;
import com.instagram.db.InstagramAccounts;

import java.util.List;

public interface CredentialsDAO {

    Credentials getCredentialsByLogin(String login);

    Credentials getCredentialsById(int id);

    String getCredentialsLoginByInstaAccount(String loginName);

    long createNewAccount(Credentials newAccount);

    List<Credentials> listUserCredentials();

    void removeAccountByLogin(String login);

    void updatePassword(Credentials newAccount);
}
