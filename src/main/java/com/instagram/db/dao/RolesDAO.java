package com.instagram.db.dao;

import com.instagram.db.Roles;

public interface RolesDAO {

    Roles getRoleById(long id);

    Roles getRoleByName(String roleName);

    long createNewRole(Roles role);

    void deleteRoleById(long id);

    void updateRoleData(Roles role);
}
