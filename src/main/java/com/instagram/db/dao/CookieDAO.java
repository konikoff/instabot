package com.instagram.db.dao;

import com.instagram.db.Cookie;
import com.instagram.db.InstagramAccounts;

public interface CookieDAO {

    void saveCookies(Cookie cookie);

    void deleteCookies(int id);

    Cookie getCookies(int id);

    Cookie getCookiesByInstaAccount(InstagramAccounts account);
}
