package com.instagram.db.dao;

import com.instagram.db.Credentials;
import com.instagram.db.InstagramAccounts;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Create new user by login and password
 */
@Repository
public class CredentialsDAOImpl implements CredentialsDAO {

    private static final Logger logger = LoggerFactory.getLogger(CredentialsDAOImpl.class);

    private SessionFactory sessionFactory;

    public CredentialsDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Credentials getCredentialsByLogin(String login) {
        Session session = this.sessionFactory.getCurrentSession();
        Query getCredentialsByLoginQuery = session.createQuery("FROM Credentials WHERE login=:login");
        getCredentialsByLoginQuery.setParameter("login", login);
        Credentials credentials = (Credentials) getCredentialsByLoginQuery.uniqueResult();
        logger.info("Credential loaded, Credentials=" + credentials);
        return credentials;
    }

    @Override
    public Credentials getCredentialsById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Credentials userCredentials = session.load(Credentials.class, id);
        return userCredentials;
    }

    @Override
    public String getCredentialsLoginByInstaAccount(String loginName) {
        Session session = this.sessionFactory.getCurrentSession();
        Query getLoginByInstaAccount = session.createQuery("SELECT CR.login FROM Credentials AS CR, InstagramAccounts AS ACCOUNT WHERE CR = ACCOUNT.credentials AND ACCOUNT.loginName=:loginName");
        getLoginByInstaAccount.setParameter("loginName", loginName);
        String credentialsLogin = (String) getLoginByInstaAccount.uniqueResult();
        logger.info("Found login=" + credentialsLogin);
        return credentialsLogin;
    }

    @Override
    public long createNewAccount(Credentials newAccount) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(newAccount);
        logger.info("New account was successfully added, Credentials=" + newAccount);
        return newAccount.getId();
    }

    @Override
    public List<Credentials> listUserCredentials() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Credentials> credentialsList = session.createQuery("from Credentials").list();
        for (Credentials credentials : credentialsList) {
            logger.info("Credentials List::" + credentials);
        }
        return credentialsList;
    }

    @Override
    public void removeAccountByLogin(String login) {

    }

    @Override
    public void updatePassword(Credentials newAccount) {

    }
}
