package com.instagram.db;

import javax.persistence.*;

@Table(name = "USER_ROLES")
@Entity
public class UserRoles {

    @Id
    @Column(name = "USERROLES_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userRoleId;

    @OneToOne
    @JoinColumn(name = "login_id", nullable = false, unique = true)
    private Credentials credentials;

    @OneToOne
    @JoinColumn(name = "role_id", nullable = false)
    private Roles role;

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserRoles{" +
                "userRoleId=" + userRoleId +
                ", credentials=" + credentials +
                ", role=" + role +
                '}';
    }
}
