package com.instagram.db;

import javax.persistence.*;

@Entity
@Table(name = "INSTAGRAM_ACCOUNTS", uniqueConstraints = {@UniqueConstraint(columnNames = {"LOGIN", "credential_id"})})
public class InstagramAccounts {

    @Id
    @Column(name = "inst_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "LOGIN", nullable = false)
    private String loginName;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @ManyToOne
    @JoinColumn(name = "credential_id", nullable = false)
    private Credentials credentials;

    @OneToOne(mappedBy = "instagramAccounts")
    private Cookie cookie;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public Cookie getCookie() {
        return cookie;
    }

    public void setCookie(Cookie cookie) {
        this.cookie = cookie;
    }
}
