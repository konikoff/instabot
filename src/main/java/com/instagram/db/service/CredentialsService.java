package com.instagram.db.service;

import com.instagram.db.Credentials;

import java.util.List;

public interface CredentialsService {
    Credentials getCredentialsByLogin(String login);

    String getCredentialsLoginByInstaAccount(String loginName);

    long createNewAccount(Credentials newAccount);

    List<Credentials> listUserCredentials();

    void removeAccountByLogin(String login);

    void updatePassword(Credentials newAccount);

    Credentials getCredentialsById(int id);
}
