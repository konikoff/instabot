package com.instagram.db.service;

import com.instagram.db.Credentials;
import com.instagram.db.InstagramAccounts;

import java.util.List;

public interface InstagramAccountService {
    void saveOrUpdateAccount(InstagramAccounts accounts);

    InstagramAccounts getAccount(int id);

    InstagramAccounts getAccountByLogin(String login);

    void deleteAccount(int id);

    List<InstagramAccounts> getAccountsBelongToUser(Credentials credentials);
}
