package com.instagram.db.service;

import com.instagram.db.User;
import com.instagram.db.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User service to manage user data
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public User getUserById(long id) {
        return this.userDAO.getUserById(id);
    }

    @Override
    @Transactional
    public long createNewUser(User newUser) {
        return this.userDAO.createNewUser(newUser);
    }

    @Override
    @Transactional
    public void deleteUserById(long id) {
        this.userDAO.deleteUserById(id);
    }

    @Override
    @Transactional
    public void updateUserData(User user) {
        this.userDAO.updateUserData(user);
    }
}
