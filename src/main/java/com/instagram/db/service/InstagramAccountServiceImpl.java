package com.instagram.db.service;

import com.instagram.db.Credentials;
import com.instagram.db.InstagramAccounts;
import com.instagram.db.dao.InstagramAccountDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("instagramAccountService")
public class InstagramAccountServiceImpl implements InstagramAccountService {

    @Autowired
    private InstagramAccountDAO instagramAccount;

    @Override
    @Transactional
    public void saveOrUpdateAccount(InstagramAccounts accounts) {
        instagramAccount.saveOrUpdateAccount(accounts);
    }

    @Override
    @Transactional
    public InstagramAccounts getAccount(int id) {
        return instagramAccount.getAccount(id);
    }

    @Override
    public InstagramAccounts getAccountByLogin(String login) {
        return instagramAccount.getAccountByLogin(login);
    }

    @Override
    @Transactional
    public void deleteAccount(int id) {
        instagramAccount.deleteAccount(id);
    }

    @Override
    @Transactional
    public List<InstagramAccounts> getAccountsBelongToUser(Credentials credentials) {
        return instagramAccount.getAccountsBelongToUser(credentials);
    }
}
