package com.instagram.db.service;

import com.instagram.db.Roles;

public interface RolesService {
    Roles getRoleById(long id);

    Roles getRoleByName(String roleName);

    long createNewRole(Roles role);

    void deleteRoleById(long id);

    void updateRoleData(Roles role);
}
