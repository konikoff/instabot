package com.instagram.db.service;

import com.instagram.db.InstagramAccounts;
import com.instagram.db.dao.CookieDAO;
import org.openqa.selenium.Cookie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

@Service("cookieService")
public class CookieServiceImpl implements CookieService {

    private final CookieDAO cookieDAO;

    @Autowired
    public CookieServiceImpl(CookieDAO cookieDAO) {
        this.cookieDAO = cookieDAO;
    }

    @Override
    @Transactional
    public void saveCookies(com.instagram.db.Cookie cookie, Set<Cookie> cookieSet, InstagramAccounts accounts) {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(byteArrayOutputStream)) {

            oos.writeObject(cookieSet);

            cookie.setCookies(byteArrayOutputStream.toByteArray());
            cookie.setInstagramAccounts(accounts);
            cookieDAO.saveCookies(cookie);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void deleteCookies(int id) {
        cookieDAO.deleteCookies(id);
    }


    @Override
    @Transactional
    public Set<Cookie> getCookies(int id) {
        Set<Cookie> cookieSet = null;
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(cookieDAO.getCookies(id).getCookies());
             ObjectInputStream ois = new ObjectInputStream(byteArrayInputStream)) {

            cookieSet = (HashSet<Cookie>) ois.readObject();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return cookieSet;
    }

    @Override
    @Transactional
    public com.instagram.db.Cookie getCookiesByInstaAccount(InstagramAccounts account) {
        return cookieDAO.getCookiesByInstaAccount(account);
    }


}
