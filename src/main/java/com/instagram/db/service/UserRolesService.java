package com.instagram.db.service;

import com.instagram.db.Credentials;
import com.instagram.db.UserRoles;

public interface UserRolesService {

    UserRoles getRoleForSpecificUser(Credentials credentials);

    long assignRoleToUser(UserRoles userRole);

    void deleteRoleFromUserById(long id);

    void updateUserRole(UserRoles userRole);
}
