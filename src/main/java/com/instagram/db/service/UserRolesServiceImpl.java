package com.instagram.db.service;

import com.instagram.db.Credentials;
import com.instagram.db.UserRoles;
import com.instagram.db.dao.UserRolesDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userRolesService")
public class UserRolesServiceImpl implements UserRolesService {

    @Autowired
    private UserRolesDAO userRolesDAO;

    @Override
    @Transactional
    public UserRoles getRoleForSpecificUser(Credentials credentials) {
        return userRolesDAO.getRoleForSpecificUser(credentials);
    }

    @Override
    @Transactional
    public long assignRoleToUser(UserRoles userRole) {
        return userRolesDAO.assignRoleToUser(userRole);
    }

    @Override
    @Transactional
    public void deleteRoleFromUserById(long id) {
        userRolesDAO.deleteRoleFromUserById(id);
    }

    @Override
    @Transactional
    public void updateUserRole(UserRoles userRole) {
        userRolesDAO.updateUserRole(userRole);
    }
}
