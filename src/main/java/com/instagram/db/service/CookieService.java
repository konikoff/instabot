package com.instagram.db.service;

import com.instagram.db.InstagramAccounts;
import org.openqa.selenium.Cookie;

import java.util.Set;

public interface CookieService {
    void saveCookies(com.instagram.db.Cookie cookie, Set<Cookie> cookieSet, InstagramAccounts accounts);

    void deleteCookies(int id);

    Set<Cookie> getCookies(int id);

    com.instagram.db.Cookie getCookiesByInstaAccount(InstagramAccounts account);
}
