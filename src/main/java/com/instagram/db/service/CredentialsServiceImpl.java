package com.instagram.db.service;

import com.instagram.db.Credentials;
import com.instagram.db.dao.CredentialsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("credentialsService")
public class CredentialsServiceImpl implements CredentialsService {

    @Autowired
    private CredentialsDAO credentialsDAO;

    @Override
    @Transactional
    public Credentials getCredentialsByLogin(String login) {
        return this.credentialsDAO.getCredentialsByLogin(login);
    }

    @Override
    @Transactional
    public String getCredentialsLoginByInstaAccount(String loginName) {
        return credentialsDAO.getCredentialsLoginByInstaAccount(loginName);
    }

    @Override
    @Transactional
    public long createNewAccount(Credentials newAccount) {
        return this.credentialsDAO.createNewAccount(newAccount);
    }

    @Override
    @Transactional
    public List<Credentials> listUserCredentials() {
        return this.credentialsDAO.listUserCredentials();
    }

    @Override
    @Transactional
    public void removeAccountByLogin(String login) {
        this.credentialsDAO.removeAccountByLogin(login);
    }

    @Override
    @Transactional
    public void updatePassword(Credentials newAccount) {
        this.credentialsDAO.updatePassword(newAccount);
    }

    @Override
    @Transactional
    public Credentials getCredentialsById(int id) {
        return this.credentialsDAO.getCredentialsById(id);
    }
}
