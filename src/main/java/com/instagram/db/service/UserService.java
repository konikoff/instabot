package com.instagram.db.service;

import com.instagram.db.User;

public interface UserService {
    User getUserById(long id);

    long createNewUser(User newUser);

    void deleteUserById(long id);

    void updateUserData(User user);
}
