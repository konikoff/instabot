package com.instagram.db.service;

import com.instagram.db.Roles;
import com.instagram.db.dao.RolesDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("roleService")
public class RolesServiceImpl implements RolesService {

    @Autowired
    private RolesDAO rolesDAO;


    @Override
    @Transactional
    public Roles getRoleById(long id) {
        return rolesDAO.getRoleById(id);
    }

    @Override
    @Transactional
    public Roles getRoleByName(String roleName) {
        return rolesDAO.getRoleByName(roleName);
    }

    @Override
    @Transactional
    public long createNewRole(Roles role) {
        return rolesDAO.createNewRole(role);
    }

    @Override
    @Transactional
    public void deleteRoleById(long id) {
        rolesDAO.deleteRoleById(id);
    }

    @Override
    @Transactional
    public void updateRoleData(Roles role) {
        rolesDAO.updateRoleData(role);
    }
}
