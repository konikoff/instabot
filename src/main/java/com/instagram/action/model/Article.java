package com.instagram.action.model;

import java.util.Objects;

public class Article {

    private String innerHtml;
    private LikeButton likeButton;

    public LikeButton getLikeButton() {
        return likeButton;
    }

    public void setLikeButton(LikeButton likeButton) {
        this.likeButton = likeButton;
    }

    public String getInnerHtml() {
        return innerHtml;
    }

    public void setInnerHtml(String innerHtml) {
        this.innerHtml = innerHtml;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Article)) return false;
        Article article = (Article) o;
        return Objects.equals(getInnerHtml(), article.getInnerHtml()) &&
                Objects.equals(getLikeButton(), article.getLikeButton());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInnerHtml(), getLikeButton());
    }
}
