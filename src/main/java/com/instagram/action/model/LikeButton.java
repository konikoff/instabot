package com.instagram.action.model;

import java.util.Objects;

public class LikeButton {
    private Color buttonColor;

    public Color getButtonColor() {
        return buttonColor;
    }

    public void setButtonColor(Color buttonColor) {
        this.buttonColor = buttonColor;
    }

    public enum Color {
        RED, GRAY
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LikeButton)) return false;
        LikeButton that = (LikeButton) o;
        return getButtonColor() == that.getButtonColor();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getButtonColor());
    }
}
