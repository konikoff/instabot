package com.instagram.action.service;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Collection;
import java.util.Set;

public class ConsoleImpl implements Console{
    private WebDriver driver;

    public ConsoleImpl() {
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
        this.driver = new FirefoxDriver();
    }


    @Override
    public WebDriver openConnection() {
        return driver;
    }

    /**
     * Retrieve cookies from data base
     */
    @Override
    public <T extends Collection> void setCookies(T collection) {
        collection.forEach(cookie -> {
            if (cookie instanceof Cookie) {
                driver.manage().addCookie((Cookie) cookie);
            }
        });
    }

    /**
     * Save cookies after using
     */
    @Override
    public Set<Cookie> getCookies() {
        return driver.manage().getCookies();
    }

    @Override
    public void close() throws Exception {
        driver.close();
    }
}
