package com.instagram.action.service;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.util.Collection;
import java.util.Set;

public interface Console extends AutoCloseable {
    <T extends Collection> void setCookies(T collection);

    Set<Cookie> getCookies();

    WebDriver openConnection();
}
