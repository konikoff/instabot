package com.instagram.action.service;

import com.instagram.action.Command;

public interface ExecuteService {
    <T extends Command> void executeCommands(T... commands);
}
