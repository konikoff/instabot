package com.instagram.action.service;

import com.instagram.action.Command;
import org.springframework.stereotype.Service;

@Service("instagramActionService")
public class ExecuteServiceImpl implements ExecuteService {

    @Override
    public <T extends Command> void executeCommands(T... commands) {
        for (T command : commands) {
            command.execute();
        }
    }
}
