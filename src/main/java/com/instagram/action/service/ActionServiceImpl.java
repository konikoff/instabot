package com.instagram.action.service;

import com.instagram.action.LikeCommand;
import com.instagram.db.InstagramAccounts;
import com.instagram.db.service.CookieService;
import com.instagram.db.service.CredentialsService;
import com.instagram.db.service.InstagramAccountService;
import com.instagram.util.Utilities;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service("actionService")
public class ActionServiceImpl implements ActionService {
    private static final Log logger = LogFactory.getLog(ActionServiceImpl.class);

    private static final String INSTAGRAM = "https://www.instagram.com/";

    @Autowired
    private CookieService cookieService;

    @Autowired
    private CredentialsService credentialsService;

    @Autowired
    private InstagramAccountService instagramAccountService;

    @Autowired
    private ExecuteService executeService;

    @Override
    public void like(InstagramAccounts account) {
        try (Console console = new ConsoleImpl()) {
            WebDriver driver = console.openConnection();
            driver.get(INSTAGRAM);
            com.instagram.db.Cookie cookie = cookieService.getCookiesByInstaAccount(account);
            Set<Cookie> cookies = Utilities.bytesToCookies(cookie.getCookies());
            console.setCookies(cookies);

            // Refresh web page after setting cookies
            driver.navigate().refresh();

            String user = credentialsService.getCredentialsLoginByInstaAccount(account.getLoginName());

            executeService.executeCommands(new LikeCommand(driver, user, 0));
            // Save cookies

            cookieService.saveCookies(cookie, console.getCookies(), account);
        } catch (Exception e) {
            logger.error(e);
        }

    }

    @Override
    public void unlike(InstagramAccounts account) {

    }

    @Override
    public void follow(InstagramAccounts account) {

    }

    @Override
    public void unfollow(InstagramAccounts account) {

    }


}
