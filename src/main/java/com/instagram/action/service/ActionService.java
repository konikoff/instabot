package com.instagram.action.service;

import com.instagram.db.InstagramAccounts;

public interface ActionService {
    void like(InstagramAccounts account);
    void unlike(InstagramAccounts account);
    void follow(InstagramAccounts account);
    void unfollow(InstagramAccounts account);
}
