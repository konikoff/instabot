package com.instagram.action;

/**
 * Interface for all commands: Like, Follow...
 */
public interface Command {

    void execute();
}
