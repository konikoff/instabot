package com.instagram.action.event;

public interface SendMessage {
    void sendMessage(Object object);
}
