package com.instagram.action.event;

public interface EventListener {
    void update(String eventType, Object object);
}
