package com.instagram.action.event;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.util.MimeTypeUtils;

import java.util.HashMap;
import java.util.Map;

public class SendMessageImpl implements SendMessage {
    private static final Log logger = LogFactory.getLog(SendMessageImpl.class);
    private final String user;

    public SendMessageImpl(String user) {
        this.user = user;
    }

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Override
    public void sendMessage(Object object) {
        Map<String, Object> map = new HashMap<>();
        map.put(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON);
        logger.debug("Perform callback action: " + object);
        this.messagingTemplate.convertAndSendToUser(user, "/queue/like-follow", object, map);
    }
}
