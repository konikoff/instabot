package com.instagram.action;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class AbstractCommand implements Command {
    private static final Log logger = LogFactory.getLog(AbstractCommand.class);
    private int count;

    public AbstractCommand(int count) {
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
        this.count = count;
    }

    protected void scrollIntoView(WebDriver driver, WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
    }

    protected void scrollToCoordinate(WebDriver driver, int x, int y) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(String.format("window.scrollBy(%s, %s)", x, y));
    }


    @Override
    public void execute() {
        action(count);
    }

    public abstract void action(int count);
}
