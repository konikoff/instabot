package com.instagram.action;

import com.instagram.util.WeakHashSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;


/**
 * Like user posts
 */
public class LikeCommand extends AbstractCommand {
    private static final Log logger = LogFactory.getLog(LikeCommand.class);

    private static final int TIMEOUT_SEC = 600;
    private WebDriver driver;
    private String user;


    public LikeCommand(WebDriver driver, String user, int count) {
        super(count);
        this.driver = driver;
        this.user = user;
    }


    @Override
    public void action(int count) {
        WeakHashSet articlesWeakSet = new WeakHashSet();

        // 1. get articles
        List<WebElement> articleSet = loadArticles(driver);
        long timeIn5minutes = new Date().getTime() + 5 * 1000 * 60;
        //2. loop and put used article to weakSet
        while (timeIn5minutes > new Date().getTime()) {
            try {
                if (articleSet.size() > 0) {
                    for (WebElement articleElement : articleSet) {
                        if (!articlesWeakSet.contains(articleElement)) {
                            scrollIntoView(driver, articleElement);

                            articlesWeakSet.add(articleElement);

                            like(articleElement);
                            //FIXME: 3. delete entry from set
                        }
                    }
                }
            } catch (RuntimeException e) {
                logger.error("RuntimeException" + e.getMessage());
            }

            articleSet = loadArticles(driver);
        }

    }

    private List<WebElement> loadArticles(WebDriver driver) {
        WebDriverWait waiter = new WebDriverWait(driver, TIMEOUT_SEC);
        waiter.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.tagName("article"), 2));

        return driver.findElements(By.tagName("article"));

    }

    private void like(WebElement element) {
        try {
            String innerHtml = element.getAttribute("innerHTML");
            if (innerHtml.toLowerCase().contains("grey")) { //If the heart is unliked
                WebElement likeButton = element.findElement(By.className("coreSpriteHeartOpen"));

                likeButton.click();

                List<WebElement> listOfImages = element.findElements(By.tagName("img"));
                for (WebElement image : listOfImages) {
                    if (!image.getAttribute("srcset").isEmpty()) {
                        logger.info(image.getAttribute("srcset"));
                    }
                }
                // WebElement image = article.findElement(By.xpath("//*[@id=\"react-root\"]/section/main/section/div[1]/div[1]/div/article[" + i + "]/div[1]/div/div/div[1]/img"));
                //FIXME: send message  new SendMessageImpl(user).sendMessage("Liked" + image.getAttribute("srcset"));
                Thread.sleep(2000);
            }
        } catch (StaleElementReferenceException e) {
            logger.error("Caught StaleElementReferenceException:" + e.getMessage());
        } catch (InterruptedException e) {
            logger.error("Caught InterruptedException:" + e.getMessage());
        }
    }
}
