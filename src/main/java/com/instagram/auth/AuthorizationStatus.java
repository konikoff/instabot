package com.instagram.auth;

public class AuthorizationStatus {

    private Status status;
    private String message;
    private int statusCode;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
        this.statusCode = status.getCode();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public enum Status {
        DUPLICATED(100), SUCCESSFUL(200), UNSUCCESSFUL(400);

        private int code;

        Status(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
}
