package com.instagram.auth.service;


import com.instagram.auth.AuthorizationStatus;
import com.instagram.web.model.instagram.InstagramForm;

import java.security.Principal;

public interface InstagramAuthentication {
    AuthorizationStatus login(InstagramForm signInForm, Principal principal);

    void logout(Principal principal);
}
