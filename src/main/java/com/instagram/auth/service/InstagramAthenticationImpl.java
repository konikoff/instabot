package com.instagram.auth.service;


import com.instagram.auth.AuthorizationStatus;
import com.instagram.db.Cookie;
import com.instagram.db.Credentials;
import com.instagram.db.InstagramAccounts;
import com.instagram.db.service.CookieService;
import com.instagram.db.service.CredentialsService;
import com.instagram.db.service.InstagramAccountService;
import com.instagram.web.model.instagram.InstagramForm;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service("instagramAthenticationService")
public class InstagramAthenticationImpl implements InstagramAuthentication {

    @Autowired
    private CookieService cookieService;

    @Autowired
    private CredentialsService credentialsService;

    @Autowired
    private InstagramAccountService instagramAccountService;

    private static final Log logger = LogFactory.getLog(InstagramAthenticationImpl.class);

    private static final String LOGIN_CLASSIC = "https://www.instagram.com/accounts/login/";
    private WebDriver driver;
    private static final int TIMEOUT_SEC = 10;


    public InstagramAthenticationImpl() {
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
    }


    @Override
    public AuthorizationStatus login(InstagramForm signInForm, Principal principal) {
        this.driver = new FirefoxDriver();
        this.driver.get(LOGIN_CLASSIC);
        WebElement usernameElement = (new WebDriverWait(driver, TIMEOUT_SEC))
                .until(ExpectedConditions.presenceOfElementLocated(By.name("username")));
        usernameElement.sendKeys(signInForm.getLogin());

        WebElement passwordElement = (new WebDriverWait(driver, TIMEOUT_SEC))
                .until(ExpectedConditions.presenceOfElementLocated(By.name("password")));
        passwordElement.sendKeys(signInForm.getPassword());
        this.driver.findElement(By.xpath("//*[@id=\"react-root\"]/section/main/div/article/div/div[1]/div/form/span/button")).click();

        new WebDriverWait(driver, TIMEOUT_SEC).until(ExpectedConditions.presenceOfElementLocated(By.className("coreSpriteDesktopNavProfile")));

        Credentials credentials = credentialsService.getCredentialsByLogin(principal.getName());

        InstagramAccounts account = new InstagramAccounts();
        account.setLoginName(signInForm.getLogin());
        account.setPassword(signInForm.getPassword());//Obfuscate password
        account.setCredentials(credentials);

        AuthorizationStatus status = new AuthorizationStatus();

        try {
            WebElement loggedInElement = (new WebDriverWait(driver, TIMEOUT_SEC)).until(ExpectedConditions.presenceOfElementLocated(By.className("logged-in")));
            if (loggedInElement != null) {
                logger.debug("User successfully authorized as: " + signInForm.getLogin());
                status.setStatus(AuthorizationStatus.Status.SUCCESSFUL);
                status.setMessage("You successfully authorized as: " + signInForm.getLogin());
                try {
                    instagramAccountService.saveOrUpdateAccount(account);
                } catch (ConstraintViolationException e) {
                    logger.info("User already has this account. Account: " + signInForm.getLogin());
                    status.setMessage("User already has this account. Account: " + signInForm.getLogin());
                    status.setStatus(AuthorizationStatus.Status.DUPLICATED);
                    this.driver.close();
                    return status;
                }
                cookieService.saveCookies(new Cookie(), driver.manage().getCookies(), account);
            } else {
                logger.debug("User unauthorized as: " + signInForm.getLogin());
                status.setStatus(AuthorizationStatus.Status.UNSUCCESSFUL);
                status.setMessage("You isn't authorized as: " + signInForm.getLogin());
            }
        } catch (TimeoutException e) {
            logger.error("Timeout exception seems that user isn't authorized");
            status.setStatus(AuthorizationStatus.Status.UNSUCCESSFUL);
            status.setMessage("You isn't authorized as: " + signInForm.getLogin());
        } finally {
            this.driver.close();
        }

        return status;
    }

    @Override
    public void logout(Principal principal) { //TODO: Specify instagram login
        Credentials credentials = credentialsService.getCredentialsByLogin(principal.getName());
        cookieService.deleteCookies(1);


    }

}
