-- Credentials table
CREATE TABLE credentials
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    login varchar(30) NOT NULL,
    password blob NOT NULL
);
CREATE UNIQUE INDEX credentials_id_uindex ON credentials (id);
CREATE UNIQUE INDEX credentials_login_uindex ON credentials (login);

-- User data
CREATE TABLE INSTABOT.INSTABOT.USER (
  user_id   INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  FirstName VARCHAR2(20),
  LastName  VARCHAR2(20),
  Email     VARCHAR2(50) NOT NULL,
  LoginName VARCHAR2(30) UNIQUE NOT NULL
);